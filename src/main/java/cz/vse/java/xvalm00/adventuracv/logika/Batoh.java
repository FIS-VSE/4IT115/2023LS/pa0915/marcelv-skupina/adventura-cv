package cz.vse.java.xvalm00.adventuracv.logika;

import cz.vse.java.xvalm00.adventuracv.observer.Observable;
import cz.vse.java.xvalm00.adventuracv.observer.Observer;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;

public class Batoh implements Observable {

    private Map<String, Vec> seznamVeci;
    private Set<Observer> observers;

    public static final int KAPACITA = 3;

    public Batoh() {
        seznamVeci = new HashMap<String, Vec>();
        observers = new HashSet<>();
    }

    public void vlozVec(Vec vec) {
        seznamVeci.put(vec.getJmeno(), vec);
        notifyObservers();
    }

    public Vec vyberVec(String jmeno) {
        Vec nalezenaVec = null;
        if (seznamVeci.containsKey(jmeno)) {
            nalezenaVec = seznamVeci.get(jmeno);
            seznamVeci.remove(jmeno);
            notifyObservers();
        }
        return nalezenaVec;
    }

    public boolean obsahujeVec(String jmeno) {
        return seznamVeci.containsKey(jmeno);
    }

    public String nazvyVeci() {
        StringBuilder nazvy = new StringBuilder("věci v batohu: ");
        for (String jmenoVeci : seznamVeci.keySet()) {
            nazvy.append(jmenoVeci).append(" ");
        }
        return nazvy.toString();
    }

    public Set<String> getMnozinaVeci() {
        return seznamVeci.keySet();
    }

    public int getPocetVeci() {
        return seznamVeci.size();
    }

    public boolean jeMistoVBatohu() {
        return getPocetVeci() < KAPACITA;
    }

    @Override
    public void register(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unregister(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    public Map<String, Vec> getSeznamVeci() {
        return seznamVeci;
    }
}
