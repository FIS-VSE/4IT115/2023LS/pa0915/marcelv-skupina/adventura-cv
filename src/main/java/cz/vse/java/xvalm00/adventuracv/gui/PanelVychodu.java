package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.observer.Observer;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;

import java.util.Collection;

public class PanelVychodu implements Observer {

    ListView<String> listView = new ListView<>();

    private HerniPlan herniPlan = Hra.getSingleton().getHerniPlan();
    private Hra hra = Hra.getSingleton();

    public PanelVychodu() {
        init();
        aktualizuj();
        herniPlan.register(this);
    }

    private void init() {
        listView.setMaxWidth(150);
        listView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    String selectedProstor = listView.getSelectionModel().getSelectedItem();
                    if (selectedProstor != null) {
                        String prikaz = "běž " + selectedProstor;
                        hra.zpracujPrikaz(prikaz);
                    }
                }
            }
        });
    }

    private void aktualizuj() {
        listView.getItems().clear();
        Collection<Prostor> vychody = herniPlan.getAktualniProstor().getVychody();
        for (Prostor prostor : vychody) {
            String nazev = prostor.getNazev();
            listView.getItems().add(nazev);
        }
    }

    public ListView<String> getListView() {
        return listView;
    }

    @Override
    public void update() {
        aktualizuj();
    }
}